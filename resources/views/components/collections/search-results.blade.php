<div class="tw-flex tw-justify-between tw-items-center">
    <div>
        <h1 class="tw-text-xl tw-font-bold">{{ $collection->search_title }}</h1>
    </div>
    <div>
        <button class="tw-cursor-pointer">
            <span class="tw-text-sm">Sort By</span>
            <span class="tw-text-sm tw-font-bold tw-ml-1">Price Low To High</span>
        </button>
        <span class="inline-block tw-ml-4 lg:tw-hidden">
            <svg class="icon icon-filter tw-text-4xl tw-text-primary-600"><use xlink:href="#icon-filter"></use></svg>
        </span>
    </div>
</div>
<div class="tw-mt-4">
    <div class="tw-flex tw-flex-wrap" style="margin:-10px;">
        @foreach($collection->packages as $package)
        <div class="tw-w-full sm:tw-w-1/2 xl:tw-w-1/3" style="padding:10px">
            <div class="card tw-relative tw-flex tw-flex-col tw-break-words tw-shadow">
                <div class="card-image-wrapper tw-relative">
                    <a href="" class="tw-block tw-w-full tw-cursor-pointer">
                        <img src="{{$package->thumbnail}}" alt="{{$package->title}}">
                    </a>
                    <a class="tw-absolute tw-bottom-0 tw-w-full gradient tw-cursor-pointer" style="height: 56%"></a>
                    <a class="tw-absolute tw-text-white tw-bottom-0 tw-w-full tw-p-4 tw-cursor-pointer">
                        <span class="tw-text-xs tw-font-extrabold tw-uppercase">
                            <small>{{$package->tour_type}} - {{$package->code}}</small>
                        </span>
                        <h2 class="tw-font-extrabold">{{$package->title}}</h2>
                    </a>
                </div>
                <div class="card-content tw-p-4">
                    <a href="" class="tw-block tw-cursor-pointer">
                        <ul class="tw-flex tw-justify-between tw-flex-wrap">
                            <li class="tw-inline-block">
                                <span class="tw-text-xs">
                                    <small class="tw-uppercase">STARTS FROM</small>
                                </span>
                                <div>
                                    <h3 class="tw-font-extrabold tw-text-center">₹ {{$package->price_starts_from}}</h3>
                                </div>
                            </li>
                            <li class="tw-inline-block">
                                <span class="tw-text-xs">
                                    <small class="tw-uppercase">COUNTRIES</small>
                                </span>
                                <div>
                                    <h3 class="tw-font-extrabold tw-text-center">{{$package->no_of_countries}}</h3>
                                </div>
                            </li>
                            <li class="tw-inline-block">
                                <span class="tw-text-xs">
                                    <small class="tw-uppercase">DAYS</small>
                                </span>
                                <div>
                                    <h3 class="tw-font-extrabold tw-text-center">{{$package->tour_duration}}</h3>
                                </div>
                            </li>
                        </ul>
                    </a>
                    <div style="min-height:58px;">
                        <div class="tw-flex tw-flex-col">
                            <a class="tw-cursor-pointer" style="min-height:44px;">
                                &nbsp;
                            </a>
                            <a class="tw-cursor-pointer tw-text-right">
                                <span class="tw-text-gray-500 hover:tw-text-primary-600 tw-font-medium tw-text-sm">
                                    Quick View
                                    <svg class="icon icon-view-show"><use xlink:href="#icon-view-show"></use></svg>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>