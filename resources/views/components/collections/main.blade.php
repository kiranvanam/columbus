<section class="tw-mb-8 tw-px-8">
    <div>
        <span class="tw-text-sm">Showing 1 - 10 of 100 results</span>    
    </div>
    <div class="tw-flex tw-flex-nowrap">
        <div class="tw-w-full lg:tw-w-3/4">
            @component('components.collections.search-results', ['collection' => $collection])
            @endcomponent
        </div>
        <aside class="tw-hidden lg:tw-block tw-w-1/4 tw-pl-4">
            @component('components.collections.filter')
            @endcomponent
        </aside>
    </div>
</section>
@component('components.collections.information', ['collectionInformation' => $collection->information])
@endcomponent