<footer class="tw-bg-primary-600 tw-text-white tw-pt-12">
    <div class="container">
        <div>
            <div class="tw-flex tw-flex-nowrap tw-flex-col md:tw-flex-row md:-tw-mr-4">
                <div class="md:tw-pr-4 tw-w-full md:tw-w-1/2">
                    <div class="lg:tw-max-w-sm">
                        <div class="tw-mb-10">
                            <img src="/images/veenaWorld-gray-2.webp" alt="Footer Logo">
                            <p class="lg:tw-text-lg">Building a legacy of redefining travel, inspiring to explore and sharing reasons to celebrate!</p>
                        </div>
                        <div class="tw-mt-12">
                            <div class="tw-mb-2">
                                <span class="tw-text-sm">Get Latest Deals, Upcoming Offers & Travel Guides</span>
                            </div>
                            <div>
                                <form class="tw-w-full tw-max-w-sm">
                                    <div class="tw-flex tw-items-center tw-border-b-2 tw-border-white tw-py-2">
                                        <input class="tw-appearance-none tw-bg-transparent tw-border-none tw-w-full tw-text-white tw-mr-3 tw-py-1 tw-px-2 tw-leading-tight focus:tw-outline-none" type="text" placeholder="jane@doe.com" aria-label="Full name">
                                        <button class="tw-flex-shrink-0 hover:tw-bg-transparent tw-bg-white tw-text-red-600 tw-border-white tw-text-sm tw-border-2 hover:tw-text-white tw-py-1 tw-px-2 tw-rounded" type="button">
                                            Subscribe
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="md:tw-pr-4 tw-w-full md:tw-w-1/2 tw-mt-12 md:tw-mt-0">
                    <div class="tw-mb-4">
                        <div class="md:tw-w-5/6">
                            <h2 class="tw-text-2xl tw-text-gold-300">#CelebrateLife</h2>
                            <p class="tw-text-sm">        
                                At Columbus Vacations, we believe in celebrating life and that is what makes us the absolute best.
                            </p>
                        </div>
                    </div>
                    <div class="tw-flex tw-flex-wrap">
                        <div class="tw-w-full tw-py-4 sm:tw-w-1/2 md:tw-w-1/3 tw-border-b sm:tw-border-b-0 tw-mb-4 sm:tw-mb-0">
                            <h4 class="tw-uppercase tw-mb-4 tw-font-bold">Support</h4>
                            <ul class="col-count-2 col-gap-20 sm:col-count-1">
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Contact Us</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Travel Agents</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">How to Book</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">FAQ</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Enquiry</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Leave Your Feeback</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Tour Status</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Site Map</a></h5>
                                </li>
                            </ul>                           
                        </div>
                        <div class="tw-w-full tw-py-4 sm:tw-w-1/2 md:tw-w-1/3 tw-border-b sm:tw-border-b-0 tw-mb-4 sm:tw-mb-0">
                            <h4 class="tw-uppercase tw-mb-4 tw-font-bold">Resources</h4>
                            <ul class="col-count-2 col-gap-20 sm:col-count-1">
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank"></a>Blog</h5> 
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank"></a>Travel Guides</h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">How to Book</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Travel Deals</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Privacy</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Terms & Conditions</a></h5>
                                </li>
                            </ul>
                        </div>
                        <div class="tw-w-full tw-py-4 sm:tw-w-1/2 md:tw-w-1/3 tw-mb-4 sm:tw-mb-0">
                            <h4 class="tw-uppercase tw-mb-4 tw-font-bold">Company</h4>
                            <ul class="col-count-2 col-gap-20 sm:col-count-1">
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">About Us</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Leadership</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Why Columbus?</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Careers</a></h5>
                                </li>
                                <li class="tw-mb-4 sm:tw-mb-2">
                                    <h5 class="tw-text-sm"><a href="#" target="_blank">Travel Planner</a></h5>
                                </li>
                            </ul>                         
                        </div>
                    </div>
                </div>
            </div>
            <div class="tw-mt-8 md:tw-mt-0">
                <div class="tw-py-2 tw-border-t-2 tw-border-b-2 tw-border-white">
                    <div class="tw-flex tw-flex-wrap">
                        <div class="tw-w-full sm:tw-w-1/2 tw-my-auto">
                            <a target="_blank" href="#" class="tw-mr-2"><svg class="icon icon-facebook-round"><use xlink:href="#icon-facebook-round"></use></svg></a>
                            <a target="_blank" href="#" class="tw-mr-2"><svg class="icon icon-twitter"><use xlink:href="#icon-twitter"></use></svg></a>
                            <a target="_blank" href="#" class="tw-mr-2"><svg class="icon icon-youtube"><use xlink:href="#icon-youtube"></use></svg></a>
                            <a target="_blank" href="#" class="tw-mr-2"><svg class="icon icon-instagram"><use xlink:href="#icon-instagram"></use></svg></a>
                        </div>
                        <div class="tw-w-full tw-mt-2 sm:tw-mt-0 sm:tw-w-1/2">
                            <div class="tw-flex tw-flex-wrap">
                                <div class="tw-w-full lg:tw-w-1/2">
                                    <a href="tel:+914066881991" class="">
                                        Call +9140 6688 1991
                                    </a>
                                </div>
                                <div class="tw-w-full lg:tw-w-1/2 tw-mt-2 lg:tw-mt-0">
                                    <a href="mailto:travel@columbusvacations.in?Subject=New%20Enquiry">
                                        travel@columbusvacations.in
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tw-py-2 tw-text-sm tw-text-center">
                ©<?php echo date("Y"); ?> Columbus Vacations Pvt Ltd. All Rights Reserved.
            </div>
        </div>
    </div>
</footer>