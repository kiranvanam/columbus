<section class="tw-w-full tw-mb-16">
    <div class="container">
        <h2 class="tw-font-light tw-text-7xl tw-mb-6">What's Your Travel Inspiration?</h2>
        <div class="owl-inspiration owl-container-carousel owl-carousel owl-theme">
            <a target="_blank" href="www.columbusvacations.in" class="tw-block">
                <figure class="tw-relative tw-m-0 tw-overflow-hidden tw-block" style="padding-bottom: 63%">
                    <img class="tw-absolute tw-h-full tw-w-full tw-object-cover" src="//cdn.tourradar.com/s3/tour/360x210/83037_e3a8a9.jpg" alt="">
                    <div class="tw-absolute tw-bottom-0 tw-w-full gradient" style="height:70%"></div>
                    <div class="tw-absolute tw-bottom-0 tw-w-full tw-text-white tw-p-6">
                        <span class="tw-font-bold tw-text-3xl">Culture</span>
                    </div>
                </figure>
            </a>
            <a target="_blank" href="www.columbusvacations.in" class="tw-block">
                <figure class="tw-relative tw-m-0 tw-overflow-hidden tw-block" style="padding-bottom: 63%">
                    <img class="tw-absolute tw-h-full tw-w-full tw-object-cover" src="//cdn.tourradar.com/s3/review/360x210/90770_1fc1d22c.jpg" alt="">
                    <div class="tw-absolute tw-bottom-0 tw-w-full gradient" style="height:70%"></div>
                    <div class="tw-absolute tw-bottom-0 tw-w-full tw-text-white tw-p-6">
                        <span class="tw-font-bold tw-text-3xl">Adventure</span>
                    </div>
                </figure>
            </a>
            <a target="_blank" href="www.columbusvacations.in" class="tw-block">
                <figure class="tw-relative tw-m-0 tw-overflow-hidden tw-block" style="padding-bottom: 63%">
                    <img class="tw-absolute tw-h-full tw-w-full tw-object-cover" src="//cdn.tourradar.com/s3/tour/360x210/133077_6b641fd3.jpg" alt="">
                    <div class="tw-absolute tw-bottom-0 tw-w-full gradient" style="height:70%"></div>
                    <div class="tw-absolute tw-bottom-0 tw-w-full tw-text-white tw-p-6">
                        <span class="tw-font-bold tw-text-3xl">Nature</span>
                    </div>
                </figure>
            </a>
            <a target="_blank" href="www.columbusvacations.in" class="tw-block">
                <figure class="tw-relative tw-m-0 tw-overflow-hidden tw-block" style="padding-bottom: 63%">
                    <img class="tw-absolute tw-h-full tw-w-full tw-object-cover" src="//cdn.tourradar.com/s3/content-pages/210/436x336/WU3IoS.jpeg" alt="">
                    <div class="tw-absolute tw-bottom-0 tw-w-full gradient" style="height:70%"></div>
                    <div class="tw-absolute tw-bottom-0 tw-w-full tw-text-white tw-p-6">
                        <span class="tw-font-bold tw-text-3xl">Festivals & Events</span>
                    </div>
                </figure>
            </a>
            <a target="_blank" href="www.columbusvacations.in" class="tw-block">
                <figure class="tw-relative tw-m-0 tw-overflow-hidden tw-block" style="padding-bottom: 63%">
                    <img class="tw-absolute tw-h-full tw-w-full tw-object-cover" src="//cdn.tourradar.com/s3/review/360x210/90770_1fc1d22c.jpg" alt="">
                    <div class="tw-absolute tw-bottom-0 tw-w-full gradient" style="height:70%"></div>
                    <div class="tw-absolute tw-bottom-0 tw-w-full tw-text-white tw-p-6">
                        <span class="tw-font-bold tw-text-3xl">Marine</span>
                    </div>
                </figure>
            </a>
        </div>
    </div>
</section>