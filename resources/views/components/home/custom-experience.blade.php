<section class="tw-relative tw-w-full tw-py-4 tw-min-h-xs tw-bg-fixed tw-bg-cover overflow-hidden tw-mb-16" style="background-image: url('/images/vw/custom-experience.webp');">
    <div class="container tw-absolute tw-inset-0 tw-flex tw-items-center">
        <div class="tw-flex tw-flex-col tw-text-white tw-w-full md:tw-w-4/5 lg:tw-w-2/3">
            <h2 class="tw-mb-4 lg:tw-mb-8 tw-text-3xl tw-font-extrabold">
                Need a more customized experience?
                <br>
                We will make it happen.
            </h2>
            <p class="tw-mb-4 lg:tw-mb-8 tw-text-lg">
                Our customized holidays offer the perfect escape to unique destinations around the world along with exclusive experiences, designed as per your choice and planned at your pace!
            </p>
            <div>
                <a class="tw-bg-primary-600 tw-uppercase tw-cursor-pointer tw-inline-block tw-border-primary-500 tw-px-4 sm:tw-px-10 tw-py-4">
                    View Customized Holidays
                </a>
            </div>
        </div>
    </div>
</section>