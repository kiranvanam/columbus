<header class="tw-border-b">
    <div class="tw-flex tw-flex-wrap tw-items-stretch tw-items-center tw-justify-between tw-px-4">
        <div class="tw-flex tw-items-center tw-h-24">
            <a href="/" class="tw-block tw-flex-none">
                <img class="tw-hidden lg:tw-block" src="/images/full-logo.svg" style="max-height: 6rem" alt="Columbus Logo">
                <img class="lg:tw-hidden" src="/images/only-logo.svg" style="max-height: 6rem" alt="Columbus Logo">
            </a>
        </div>
        <div class="tw-flex-grow">
            <div class="tw-h-full lg:tw-flex lg:tw-flex-col lg:tw-justify-between">
                <div class="tw-pt-2 tw-flex tw-justify-end tw-items-center lg:tw-text-xs tw-font-semibold tw-h-full lg:tw-h-auto">
                    <div class="tw-mr-4">
                        <a href="tel:+914066881991">
                            <span>
                                <svg class="icon icon-phone tw-text-primary-600"><use xlink:href="#icon-phone-solid"></use></svg>
                                <span class="tw-hidden lg:tw-ml-2 md:tw-inline">+9140 6688 1991</span>
                            </span>
                        </a>
                    </div>
                    <div class="tw-mr-4">
                        <a href="mailto:travel@columbusvacations.in">
                            <span>
                                <svg class="icon icon-envelope-with-pencil tw-text-primary-600"><use xlink:href="#icon-envelope-with-pencil"></use></svg>
                                <span class="tw-hidden lg:tw-ml-2 md:tw-inline">travel@columbusvacations.in</span>
                            </span>
                        </a>
                    </div>
                    <div class="tw-mr-4 tw-relative group tw-hidden lg:tw-block">
                        <a href="#responsive-header" class="hover:tw-border-primary-600 hover:tw-border-b">
                            Help
                        </a>
                        <div class="tw-absolute tw-bg-white tw-z-10 tw-border tw-hidden group-hover:tw-block tw-shadow tw-w-48" style="right:12px">
                            <div class="tw-p-4 tw-flex tw-justify-around">
                                <div class="">
                                    <div class="tw-mb-2">
                                        <span class="tw-block tw-text-gray-600 tw-text-sm">Toll Free Number</span>
                                        <span class="tw-block">1800 22 7979</span>
                                    </div>
                                    <div class="tw-mb-2">
                                        <span class="tw-block tw-text-gray-600 tw-text-sm">Info Center Numbers</span>
                                        <span class="tw-block">+91 40 6688 1991</span>
                                        <span class="tw-block">+91 40 6688 1992</span>
                                    </div>
                                    <hr class="tw-w-full tw-h-px tw-border-b tw-bg-blue-600">
                                    <div class="tw-mb-2">
                                        <span class="tw-block tw-text-gray-600 tw-text-sm">
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="circle" class="svg-inline--fa fa-circle tw-w-2 tw-text-green-400 tw-inline-block" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path></svg>
                                            We're open today
                                        </span>
                                        <span class="tw-block tw-font-bold tw-text-center">
                                            10am - 7pm
                                        </span>
                                    </div>
                                    <div>
                                        <span class="tw-block tw-text-gray-600 tw-text-sm">
                                            <a href="" target="_blank" class="hover:tw-text-primary-600">Locate our office</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tw-mr-4 tw-hidden lg:tw-block">
                        <a href="#responsive-header">
                            Login
                        </a>
                    </div>
                    <div class="tw-mr-4 lg:tw-hidden">
                        <a href="">
                            <svg class="icon icon-menu tw-text-xl"><use xlink:href="#icon-menu">
                        </a>
                    </div>
                </div>
                <nav class="tw-hidden lg:tw-block">
                    <ul class="menu tw-hidden lg:tw-flex lg:tw-justify-center tw-text-sm tw-font-semibold">
                        @foreach($menu as $item)
                            <li class="relative group tw-pb-2 tw-mr-6 hover:tw-border-primary-600">
                                <a href="#responsive-header" class="">
                                    {{$item->name}}
                                </a>
                                @if(array_key_exists("submenu", $item) && count($item->submenu))
                                <div class="tw-absolute tw-shadow-lg tw-py-4 tw-border-t tw-border-b tw-bg-white tw-z-30 tw-hidden group-hover:tw-block" style="top: 6rem; left:0; right: 0;">
                                    <div class="container tw-text-sm">
                                        <div class="tw-w-11/12 tw-ml-auto">
                                            <div class="multiple-cols col-count-7 col-gap-sm tw-py-6">
                                                @foreach($item->submenu as $subitem)
                                                    <div class="tw-mb-4 <?php if(count($subitem->submenu) < 15) echo('avoid-break-inside'); ?>">
                                                        <div class="tw-font-bold">
                                                            <a href="" class="">{{$subitem->name}}</a>
                                                        </div>
                                                        @isset($subitem->submenu)
                                                            @foreach($subitem->submenu as $subitem2)
                                                                <div class="hover:tw-border-primary-600 tw-font-light">
                                                                    <a href="" class="">{{$subitem2->name}}</a>
                                                                </div>
                                                            @endforeach
                                                        @endisset
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="tw-flex tw-justify-center">
                                                <a class="tw-inline-block tw-cursor-pointer tw-border tw-border-primary-600 tw-bg-primary-600 tw-text-white hover:tw-bg-white hover:tw-text-primary-600 tw-px-12 tw-py-2">View All Tours</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
        </div>
        <div class="tw-hidden tw-fixed tw-bg-white tw-right-0 tw-top-0 tw-h-screen tw-w-full md:tw-w-1/2 tw-z-999 animated slideInRight tw-shadow-lg">
            <div class="tw-flex tw-flex-col">
                <div class="tw-w-full tw-flex tw-justify-between tw-p-6">
                    <div>
                        <img src="/images/only-logo.svg" style="max-height: 2rem" alt="Columbus Logo">
                    </div>
                    <svg class="icon icon-close"><use xlink:href="#icon-close"></use></svg>
                </div>
                <div class="tw-w-full tw-p-6 tw-bg-gray-200">
                    <h4 class="tw-text-2xl tw-font-bold">Welcome, Guest!</h4>
                    <h6 class="tw-text-grey-600">
                        <a href="">Login / Signup</a>
                    </h6>
                </div>
                <div class="tw-p-6">
                    @foreach($menu as $item)
                        <div class="tw-flex tw-cursor-pointer tw-flex-nowrap tw-justify-between tw-py-4 tw-border-b tw-border-gray-500 tw-items-center">
                            <span>{{$item->name}}</span>
                            @if(array_key_exists("submenu", $item) && count($item->submenu))
                                <svg class="icon icon-chevron-right"><use xlink:href="#icon-chevron-right"></use></svg>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</header>