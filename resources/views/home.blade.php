@extends('layouts.app')

@section('content')

<!-- Main Section -->
<main class="tw-flex-grow">
    
    <!-- Home Page Search Bar -->
    <div class="tw-w-full tw-py-6">
        <div class="container">
            <h1 class="sm:tw-hidden md:tw-block tw-text-3xl tw-text-center tw-font-thin tw-py-4">
                Find Your Travel Story
            </h1>
            <div class="tw-py-4">
                <form action="" class="tw-flex tw-flex-wrap tw-justify-center">
                    <div id="search-input-box" class="tw-relative tw-w-3/4 md:tw-w-1/2">
                        <svg class="icon input-icon icon-search"><use xlink:href="#icon-search"></use></svg>
                        <input type="text" class="tw-px-2 tw-bg-gray-200 tw-h-12 tw-w-full tw-bg-transparent tw-border" 
                            placeholder="Where would you like to go?" style="text-indent:25px;">
                    </div>
                    <div class="tw-w-1/4 md:tw-w-1/6">
                        <button class="tw-w-full hover:tw-bg-primary-700 tw-text-white tw-text-sm tw-bg-primary-600 tw-h-12">Let's Go</button>
                    </div>   
                </form>
            </div>
        </div>
    </div>

    <!-- Home Page Carousel -->
    @component('components.home.main-carousel')
    @endcomponent

    <!-- What's Your Travel Inspiration? -->
    @component('components.home.travel-inspiration')
    @endcomponent

    <!-- What's Your Travel Style? -->
    @component('components.home.travel-style')
    @endcomponent

    <!-- Custom Experience -->
    @component('components.home.custom-experience')
    @endcomponent

    <!-- Why Columbus -->
    @component('components.home.why-columbus', ['whyColumbus' => $whyColumbus])
    @endcomponent

    <!-- Insider Tips -->
    @component('components.home.tips')
    @endcomponent

    <!-- Customer Reviews -->
    @component('components.home.reviews')
    @endcomponent
</main>
<!-- End Of Main Section -->

@endsection