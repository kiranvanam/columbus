@extends('layouts.app')

@section('content')

<!-- Main Section -->
<main class="tw-flex-grow">
    @component('components.collections.description', ['collection' => $collection])
    @endcomponent

    @component('components.collections.main', ['collection' => $collection])
    @endcomponent
</main>
<!-- End Of Main Section -->

@endsection