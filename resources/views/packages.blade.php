@extends('layouts.app')

@section('content')

<!-- Main Section -->
<main class="tw-flex-grow">
    <section>
        <div>
            <div>
                <img src="{{$package->banner_image}}" alt="{{$package->title}}">
            </div>
        </div>
    </section>
    <div class="sticky tw-bg-white tw-z-50">
        <div class="container tw-flex tw-justify-between tw-border-b">
            <ul class="tw-flex tw--mx-4">
                <li class="tw-px-4"><a href="" style="line-height: 42px; height:51px;" class="tw-inline-block tw-py-1 tw-border-b-4 tw-border-transparent hover:tw-border-gray-400">Overview</a></li>
                <li class="tw-px-4"><a href="" style="line-height: 42px; height:51px;" class="tw-inline-block tw-py-1 tw-border-b-4 tw-border-transparent hover:tw-border-gray-400">Itinerary</a></li>
                <li class="tw-px-4"><a href="" style="line-height: 42px; height:51px;" class="tw-inline-block tw-py-1 tw-border-b-4 tw-border-transparent hover:tw-border-gray-400">Information</a></li>
                <li class="tw-px-4"><a href="" style="line-height: 42px; height:51px;" class="tw-inline-block tw-py-1 tw-border-b-4 tw-border-transparent hover:tw-border-gray-400">FAQs</a></li>
                <li class="tw-px-4"><a href="" style="line-height: 42px; height:51px;" class="tw-inline-block tw-py-1 tw-border-b-4 tw-border-transparent hover:tw-border-gray-400">Dates & Price</a></li>
            </ul>
            @if($package->tour_type == "Customized Holiday")
            <div>
                <button href="" class="tw-block">
                    Enquire Now
                </button>
            </div>    
            @endif
        </div>
    </div>
    <section>
        <div class="container tw-border-b tw-pb-16">
            <div class="tw-w-full md:tw-w-2/3">
                <p class="tw-leading-loose tw-my-8 tw-text-justify tw-text-lg tw-font-weight">
                    {{$package->overview}}
                </p>
                <div class="tw-flex tw-flex-wrap">
                    <figure class="tw-w-full sm:tw-w-1/2 lg:tw-w-7/12 xl:tw-w-5/12">
                        <img class="tw-h-auto tw-w-full" src="{{$package->overview_image}}" alt="{{$package->title}}">
                    </figure>
                    <div class="tw-w-full sm:tw-w-1/2 lg:tw-w-5/12 xl:tw-w-7/12 tw-pl-8">
                        <h2 class="tw-mb-4">Tour Highlights</h2>
                        <ul class="tw-list-disc">
                            @foreach($package->tour_highlights as $highlight)
                                <li class="tw-text-sm tw-font-bold tw-ml-2 tw-mb-2">{{$highlight}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="tw-mt-16">
        <div class="container">
            <div class="tw-flex tw-text-teal-600 tw-text-sm tw-font-bold">
                <div class="tw-mr-4">
                    <svg class="icon icon-download"><use xlink:href="#icon-download"></use></svg>
                    <a href="" class="hover:tw-underline">Download</a>
                </div>
                <div class="tw-mr-4">
                    <svg class="icon icon-print"><use xlink:href="#icon-print"></use></svg>
                    <a href="" class="hover:tw-underline">Print</a>
                </div>
                <div>
                    <svg class="icon icon-mail"><use xlink:href="#icon-mail"></use></svg>
                    <a href="" class="hover:tw-underline">Email</a>
                </div>
            </div>
            <div><h2 class="tw-text-8xl tw-font-thin">Your Itinerary</h2></div>
        </div>
    </section>
</main>
<!-- End Of Main Section -->

@endsection