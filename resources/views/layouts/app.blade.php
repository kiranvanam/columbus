<!doctype html>
<html class="tw-font-sans tw-bg-white tw-antialiased" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body class="tw-min-h-screen tw-bg-white tw-tracking-wide tw-leading-normal">
        @component('components.svg-defs')
        @endcomponent
        <div class="tw-flex tw-flex-col tw-min-h-screen">
            <!-- Header Section -->

            @header(['menu' => $menu])
            @endheader

            <!-- Main Content Section -->
            @yield('content')

            <!-- Footer Component -->
            @footer
            @endfooter
        </div>
        <script src="/js/app.js"></script>
        <script>
            $(document).ready(function(){
                $('.owl-main').owlCarousel({
                    autoplay:true,
                    autoplayTimeout:5000,
                    autoplayHoverPause: true,
                    margin: 20,
                    center: true,
                    loop: true,
                    items: 1.2,
                    nav: true,
                    dots: false,
                    navText : [
                        "<span class='group'><img class='group-hover:tw-hidden' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block' src='/images/arrowInCircleYellow.svg'></span>",
                        "<span class='group'><img class='group-hover:tw-hidden rotate-180' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block rotate-180' src='/images/arrowInCircleYellow.svg'></span>",
                    ]
                });

                $('.owl-reviews').owlCarousel({
                    autoplay:true,
                    margin: 20,
                    autoplayTimeout:3000,
                    autoplayHoverPause: true,
                    loop: true,
                    nav: true,
                    dots: false,
                    navText : [
                        "<span class='group'><img class='group-hover:tw-hidden' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block' src='/images/arrowInCircleYellow.svg'></span>",
                        "<span class='group'><img class='group-hover:tw-hidden rotate-180' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block rotate-180' src='/images/arrowInCircleYellow.svg'></span>",
                    ],
                    responsive: {
                        0: {
                            items: 1
                        },
                        576: {
                            items: 2
                        },
                        768: {
                            items: 3
                        },
                        1000: {
                            items: 4
                        }
                    }
                });
                
                $('.owl-inspiration').owlCarousel({
                    autoplay:true,
                    margin: 20,
                    autoplayTimeout:3000,
                    autoplayHoverPause: true,
                    loop: true,
                    nav: true,
                    dots: false,
                    navText : [
                        "<span class='group'><img class='group-hover:tw-hidden' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block' src='/images/arrowInCircleYellow.svg'></span>",
                        "<span class='group'><img class='group-hover:tw-hidden rotate-180' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block rotate-180' src='/images/arrowInCircleYellow.svg'></span>",
                    ],
                    responsive: {
                        0: {
                            items: 1
                        },
                        576: {
                            items: 2
                        },
                        768: {
                            items: 3
                        }
                    }
                });

                $('.owl-travel-style').owlCarousel({
                    autoplay:true,
                    margin: 20,
                    autoplayTimeout:3000,
                    autoplayHoverPause: true,
                    loop: true,
                    nav: true,
                    dots: false,
                    navText : [
                        "<span class='group'><img class='group-hover:tw-hidden' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block' src='/images/arrowInCircleYellow.svg'></span>",
                        "<span class='group'><img class='group-hover:tw-hidden rotate-180' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block rotate-180' src='/images/arrowInCircleYellow.svg'></span>",
                    ],
                    responsive: {
                        0: {
                            items: 1
                        },
                        576: {
                            items: 2
                        },
                        768: {
                            items: 3
                        },
                        1345: {
                            items: 4
                        }
                    }
                });
            });

            
        </script>
    </body>
</html>