<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

function arrayToUnorderedList($faker, $noOfListItems = 10)
{
    $finalText = "<ul>";
    $list = $faker->sentences($noOfListItems, $asText = false);
    for ($i=0; $i < count($list) ; $i++) {
        $finalText .= "<li> $list[$i]</li>";
    }
    $finalText .= "</ul>";
    return $finalText;
}

function arrayToDiv($faker) {
    $finalText = "";
    $list = $faker->paragraphs(3);
    for ($i=0; $i < count($list) ; $i++) {
        $finalText .= "<div> $list[$i]</div>";
    }
    return $finalText;
}

function getItinerary($duration, $faker) {
    $itinerary = [];
    for($i=0; $i<$duration; $i++) {
        $day = [
            'title' => $faker->words(3),
            'description' =>$faker->text(400),
            'meals' => [
                [
                    'name' => 'Breakfast',
                ],
                [
                    'name' => 'Lunch',
                ],
                [
                    'name' => 'Dinner',
                ]
            ]
        ];
        array_push($itinerary, $day);
    }
    return $itinerary;
}

$factory->define(Model::class, function (Faker $faker) {
    $name = $faker->sentence(6);
    $tour_types = ['Customized Holiday', 'Group Tour', 'Activity'];
    $itinerary = getItinerary(mt_rand(4,10), $faker);
    $traveller_min_age = mt_rand(0, 99);
    $traveller_max_age = mt_rand($traveller_min_age, 100);
    return [
        'name' => $name,
        'slug' => str_slug($name),
        'code' => strtoupper($faker->text(4)),
        'tour_type' => $tour_types[array_rand($tour_types)],
        'overview' => $faker->text(400),
        'no_of_countries' => 2,
        'no_of_cities' => 5,
        'duration' => 6,
        'start_point' => $faker->city(),
        'end_point' => $faker->city(),
        'inclusions' => arrayToUnorderedList($faker, mt_rand(8, 12)),
        'exclusions' => arrayToUnorderedList($faker, mt_rand(4, 7)),
        'remarks' => arrayToUnorderedList($faker, mt_rand(2, 7)),
        'cancellation_terms' => arrayToUnorderedList($faker, mt_rand(2, 7)),
        'special_notes' => arrayToUnorderedList($faker, mt_rand(2, 7)),
        'tour_highlights' => arrayToUnorderedList($faker, 4)
    ];
});
