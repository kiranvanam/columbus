<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('migrate', function () {
    $this->info("You are not allowed to do any migrations. Do all in columbus-admin code");
})->describe('Disable migrations');

Artisan::command('migrate:fresh', function () {
    $this->info("You are not allowed to do any migrations. Do all in columbus-admin code");
})->describe('Disable migrations');

Artisan::command('migrate:install', function () {
    $this->info("You are not allowed to do any migrations. Do all in columbus-admin code");
})->describe('Disable migrations');

Artisan::command('migrate:refresh', function () {
    $this->info("You are not allowed to do any migrations. Do all in columbus-admin code");
})->describe('Disable migrations');

Artisan::command('migrate:reset', function () {
    $this->info("You are not allowed to do any migrations. Do all in columbus-admin code");
})->describe('Disable migrations');

Artisan::command('migrate:rollback', function () {
    $this->info("You are not allowed to do any migrations. Do all in columbus-admin code");
})->describe('Disable migrations');

Artisan::command('migrate:status', function () {
    $this->info("You are not allowed to do any migrations. Do all in columbus-admin code");
})->describe('Disable migrations');

Artisan::command('make:migration', function () {
    $this->info("You are not allowed to do any migrations. Do all in columbus-admin code");
})->describe('Disable migrations');