<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CollectionsController extends Controller
{
    public function index() {
        $menu = $this->getMenuData();
        $collectionData = $this->getCollectionData();

        return view('collections', [
            'menu' => $menu,
            'collection' => $collectionData
        ]);
    }

    protected function getCollectionData() {
        $collection_path = database_path('/seeds/data/collection.json');
        $collection_string = file_get_contents($collection_path);
        return json_decode($collection_string);
    }
}