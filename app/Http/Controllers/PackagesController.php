<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PackagesController extends Controller
{
    public function index() {
        $menu = $this->getMenuData();
        $packageData = $this->getPackageData();

        return view('packages', [
            'menu' => $menu,
            'package' => $packageData
        ]);
    }

    protected function getPackageData() {
        $package_path = database_path('/seeds/data/package.json');
        $package_string = file_get_contents($package_path);
        return json_decode($package_string);
    }
}
