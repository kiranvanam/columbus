<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index() {
        $data = $this->getHomePageData();
        return view('home', $data);
    }

    protected function getHomePageData() {
        return [
            'menu' => $this->getMenuData(),
            'whyColumbus' => $this->getWhyColumbusData()
        ];
    }  

    protected function getWhyColumbusData() {
        return [
            [
                'title' => 'Value for Money',
                'icon' => 'moneybag',
                'description' => "Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches (most with free Wi-Fi) - Columbus offers a wonderful variety of transportation for a memorable travel experience."
            ],
            [
                'title' => '24x7 Support',
                'icon' => 'time',
                'description' => "Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches (most with free Wi-Fi) - Columbus offers a wonderful variety of transportation for a memorable travel experience."
            ],
            [
                'title' => 'Expert Guidance',
                'icon' => 'compass',
                'description' => "Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches (most with free Wi-Fi) - Columbus offers a wonderful variety of transportation for a memorable travel experience."
            ],
            [
                'title' => 'Accommodation',
                'icon' => 'room',
                'description' => "Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches (most with free Wi-Fi) - Columbus offers a wonderful variety of transportation for a memorable travel experience."
            ],
            [
                'title' => 'Sightseeing',
                'icon' => 'cruise',
                'description' => "Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches (most with free Wi-Fi) - Columbus offers a wonderful variety of transportation for a memorable travel experience."
            ],
            [
                'title' => 'Tours for Everybody',
                'icon' => 'group',
                'description' => "Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches (most with free Wi-Fi) - Columbus offers a wonderful variety of transportation for a memorable travel experience."
            ]
        ];
    }
}
