const mix = require('laravel-mix');
const tailwind = require('tailwindcss');
let postCssImport = require('postcss-import');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.postCss('resources/css/app.css', 'public/css', [
        postCssImport(),
        tailwind('./tailwind.config.js'),
    ])
    .autoload({ jquery: ['$', 'window.jQuery', 'jQuery'] });

//.js('resources/js/app.js', 'public/js')